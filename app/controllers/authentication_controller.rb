class AuthenticationsController < ApplicationController
  def create
    @user = User.find_by(email: authentication_params[:email])

    if @user.authenticate(authentication_params[:password])
      token = JsonWebToken.encode({ user_id: @user.id})

      render json: { token: token }
    else
      render json: { error: 'unautorized' }, status: :unautorized
    end
  end

  private

  def authentication_params
    params.require(:user).permit(:email, :password)
  end
end
