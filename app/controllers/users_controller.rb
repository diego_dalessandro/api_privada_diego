class UsersController < ApplicationController

  def create
    
    user = User.new(user_params)
    if user.save
      render json: user.except(:password_digest)
    else
      render json: user.errors.messages, status: :bad_request
    end
  end

  private

  def user_params
    params.require(:user).permit(:nickname, :email, :password)
  end

end
