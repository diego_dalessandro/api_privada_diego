class MoviesController < ApplicationController
  def index
    render json: { movies: Movie.all }
  end

  def show
    movie = Movie.find_by(id: params[:id])

    render json: movie.to_json(only: [:name, :release_year])
  end
end
